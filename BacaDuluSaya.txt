Template ini disiapkan untuk menulis disertasi di Institut Pertanian Bogor. Untuk keperluan Skripsi
dan Tesis silahkan disesuaikan dengan aturan yang dapat di download di 
http://ppki.staff.ipb.ac.id/

Sumber awalnya template ini berasal dari template UI yang disesuaikan dengan aturan dari IPB.

Template asli UI bisa didownload di sini: http://komunitas.ui.ac.id/pg/file/andreas.febrian/read/12945/template-latex-untuk-laporan-skripsithesisdisertasi

File utama: - disertasi.tex
File laporan_setting.tex mengatur data-data utama seperti judul, tgl, pembimbing dll 
sehingga akan konsisten dalam penulisan.

Bogor, Mei 2016

Agus M Soleh
Department of Statistics
Bogor Agricultural University (IPB)
http://www.stat.ipb.ac.id/